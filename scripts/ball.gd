extends KinematicBody2D

const SPEED = 360
var vel = Vector2(SPEED,SPEED);
var padAnchor

signal ball_collided

func _ready():
	padAnchor = get_node('../pad/anchor')
	self.connect("ball_collided",get_node("../TileMap"),"_on_ball_collided")
	set_process_input(true)
	
#func _input(event):
#	if event is InputEventKey:
#		if(event.scancode == KEY_W):
#			position.y -=50
#		if(event.scancode == KEY_A):
#			position.x -=50
#		if(event.scancode == KEY_S):
#			position.y +=50
#		if(event.scancode == KEY_SEMICOLON):
#			global.score = get_node('..').WinScore-1
#			position.x +=50
			
			
func _process(delta):
	rotation = self.rotation + deg2rad(90 *delta)
	pass

func _physics_process(delta):

	var collider = move_and_collide(vel * delta)

	if collider:

		if collider.collider == get_node('../pad'):
			vel = collider.position - padAnchor.get_global_position()
			vel = vel.normalized()*SPEED *1.4
		else:
			emit_signal("ball_collided",collider.get_position())
			vel = vel.bounce(collider.normal) 

		
