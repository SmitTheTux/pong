extends Node


func _on_QuitB_pressed():
	get_tree().quit()


func _on_RestartB_pressed():
	var level
	if global.level-1 < 10 :
		level = "0"+str(global.level-1)
	else:
		level = str(global.level-1)
	print("res://scenes/levels/"+level+".tscn")
	get_tree().change_scene("res://scenes/levels/"+level+".tscn")


func _on_HomeB_pressed():
	get_tree().change_scene("res://scenes/home.tscn")

func _on_nextB_pressed():
	var level
	if global.level < 10 :
		level = "0"+str(global.level)
	else:
		level = str(global.level)
	print("res://scenes/levels/"+level+".tscn")
	get_tree().change_scene("res://scenes/levels/"+level+".tscn")