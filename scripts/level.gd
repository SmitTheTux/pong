extends Node2D

onready var WinScore = get_node("TileMap").get_used_cells_by_id(1).size()


func _ready():
	global.score = 0
	get_node('TileMap').connect("lost",self,"_on_lost")

func _on_lost():
	print("loosing now")
	get_tree().change_scene("res://scenes/loose.tscn")
	queue_free()
