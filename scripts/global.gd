extends Node

var score = 0
var level = 1
var completedLevel = 0

func _ready():
	load_game()
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.pitch_scale = 1.6
	player.stream = load('res://Dino.ogg')
	player.play()

func save():
	var save_dict = {'completedLevel_': completedLevel }
	
	var file = File.new()
	file.open('user://brickBreackerGame.game',File.WRITE)
	file.store_line(to_json(save_dict))
	file.close()


func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://brickBreackerGame.game"):
		return # Error! We don't have a save to load.
	save_game.open("user://brickBreackerGame.game", File.READ)
	var save_dict = parse_json(save_game.get_as_text())
	completedLevel = save_dict['completedLevel_']
	
	save_game.close()